# WakBoite

![pipeline](https://gitlab.com/MathiusD/wakboite/badges/master/pipeline.svg)
[![Codacy Badge](https://app.codacy.com/project/badge/Grade/a3bed3e2d8904388b6522a487bf75e1a)](https://www.codacy.com/manual/MathiusD/wakboite?utm_source=gitlab.com&amp;utm_medium=referral&amp;utm_content=MathiusD/wakboite&amp;utm_campaign=Badge_Grade)

## Concept

Un bot de gestion d'évent pour Omi (Wakfu Serveur Dathura)

## Auteur

Féry Mathieu (aka Mathius)

default_target: deploy

start: launch

launch:
	@echo "Launch Bot"
	@python3 bot.py

deploy: rebuild launch

build:
	@sh build.sh

clear_log:
	@rm logs/*.log

clear:
	@echo "Clear Repo"
	@rm -rf logs own own_install.py

rebuild: clear update build

update:
	@git pull

clear_test:
	@rm -rf tests/*.sqlite3*

test: clear_test
	@python3 -m unittest

tests: rebuild test

docker_update: update
	@docker-compose build

docker_force_update: update
	@docker-compose build --no-cache

docker_up: docker_update docker_start

docker_start:
	@docker-compose up -d

docker_down:
	@docker-compose down

docker_restart: docker_down docker_start

docker_reload: docker_force_update docker_restart
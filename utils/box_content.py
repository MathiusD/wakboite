from own.own_discord import Interact, embed_create

class BoxContent(Interact):

    @staticmethod
    def createMsg(obj:object):
        msg = "%s x %s" % (obj.quantity, obj.object)
        return embed_create(msg, obj.object)
    
    @staticmethod
    def createList(objs:list):
        out = ""
        compt = 1
        for obj in objs:
            out = "%s%i - %s x %s\n" % (out, compt, obj.quantity, obj.object)
            compt += 1
        return out
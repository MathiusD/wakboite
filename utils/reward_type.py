from own.own_discord import Interact, embed_create

class RewardType(Interact):

    @staticmethod
    def createMsg(obj:object):
        return embed_create(obj.name, obj.name)
    
    @staticmethod
    def createList(objs:list):
        out = ""
        compt = 1
        for obj in objs:
            out = "%s%i - %s\n" % (out, compt, obj.name)
            compt += 1
        return out
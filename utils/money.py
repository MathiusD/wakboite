from own.own_discord import Interact, embed_create

class Money(Interact):

    @staticmethod
    def createMsg(obj:object):
        msg = "Name :%s\nEmojiId :%s" % (obj.name, obj.id_emoji)
        return embed_create(msg, obj.name)
    
    @staticmethod
    def createList(objs:list):
        out = ""
        compt = 1
        for obj in objs:
            out = "%s%i - %s\n" % (out, compt, obj.name)
            compt += 1
        return out
from own.own_discord import Interact, embed_create
from models import types

class Reward(Interact):

    @staticmethod
    def createMsg(obj:object):
        msg = "Name :%s\nConditionType :%s\nValue :%s" % (obj.name, types[obj.condition_type].name, obj.condition_value)
        return embed_create(msg, obj.name)
    
    @staticmethod
    def createList(objs:list):
        out = ""
        compt = 1
        for obj in objs:
            out = "%s%i - %s\n" % (out, compt, obj.name)
            compt += 1
        return out
from own.own_discord import Interact, embed_create
from .box_content import BoxContent
from .box_cost import BoxCost

class Box(Interact):

    @staticmethod
    def createMsg(obj:object):
        msg = "%s\nContent :\n%s\nCost :\n%s" % (obj.name, BoxContent.createList(obj.content), BoxCost.createList(obj.cost))
        return embed_create(msg, obj.name)
    
    @staticmethod
    def createList(objs:list):
        out = ""
        compt = 1
        for obj in objs:
            out = "%s%i - %s\n" % (out, compt, obj.name)
            compt += 1
        return out
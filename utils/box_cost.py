from own.own_discord import Interact, embed_create

class BoxCost(Interact):

    @staticmethod
    def baseMsg(obj:object):
        msg = "%s x %s%s" % (obj.cost, obj.money.name, ("(<:%s:%i>)" % (obj.money.name, obj.money.id_emoji) if obj.money.id_emoji != None else ""))
        return msg

    @staticmethod
    def createMsg(obj:object):
        return embed_create(BoxCost.baseMsg(obj), obj.money)
    
    @staticmethod
    def createList(objs:list):
        out = ""
        compt = 1
        for obj in objs:
            out = "%s%i - %s\n" % (out, compt, BoxCost.baseMsg(obj))
            compt += 1
        return out
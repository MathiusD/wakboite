from discord.ext import commands
from sqlite3 import Connection
import models

class BaseDBCog(commands.Cog):

    def __init__(self, db:Connection):
        self.db = db
        self.users = models.User(db)
        self.guilds = models.Guild(db)
        self.moneys = models.Money(db, self.guilds)
        self.rewards = models.Reward(db, self.moneys)
        self.boxs = models.Box(db, self.moneys)
        self.events = models.Event(db, self.users, self.rewards)

    def findOrCreateGuild(self, guildId:int):
        guild = self.guilds.fetchGuildByDiscordId(guildId)
        if guild == None:
            guild = self.guilds.addGuild(guildId)
        return guild

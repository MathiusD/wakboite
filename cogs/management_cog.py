from discord.ext import commands
from discord import Emoji
from own import own_discord
from cogs import BaseDBCog
from utils import Box, Money, RewardType, Reward, BoxContent, BoxCost
from models import types

class ManagementCog(BaseDBCog):

    @commands.command()
    @commands.guild_only()
    @commands.has_guild_permissions(administrator=True)
    async def createBox(self, ctx:commands.Context, name:str):
        box = self.boxs.addBox(self.findOrCreateGuild(ctx.guild.id).Guild, name)
        await own_discord.reponse_send(ctx, "Box created  with name : %s !" % name if box != None else "Error as occured !")

    @commands.command()
    @commands.guild_only()
    @commands.has_guild_permissions(administrator=True)
    async def createMoney(self, ctx:commands.Context, name:str, emoji:Emoji = None):
        money = self.moneys.addMoney(self.findOrCreateGuild(ctx.guild.id).Guild, name, emoji.id if emoji != None else None)
        await own_discord.reponse_send(ctx, "Money created  with name : %s !" % name if money != None else "Error as occured !")
    

    @commands.command()
    @commands.guild_only()
    @commands.has_guild_permissions(administrator=True)
    async def createReward(self, ctx:commands.Context, name:str, valueRelated:int):
        typeSelected = await RewardType.select(ctx, types)
        if typeSelected != None:
            if self.rewards.addReward(self.findOrCreateGuild(ctx.guild.id).Guild, name, typeSelected, valueRelated) != None:
                await own_discord.reponse_send(ctx, "Reward register")
            else:
                await own_discord.reponse_send(ctx, "Error occured")

    @commands.command()
    @commands.guild_only()
    @commands.has_guild_permissions(administrator=True)
    async def addBoxContent(self, ctx:commands.Context, content:str, quantity:int = 1, coef:int = 1):
        if len(content) == 0:
            await own_discord.reponse_send(ctx, "Content must be no empty.")
        else:
            boxs = self.boxs.fetchBoxOfGuild(self.findOrCreateGuild(ctx.guild.id).Guild)
            box = await Box.select(ctx, boxs)
            if box != None:
                if self.boxs.addBoxContent(box.id, content, quantity, coef) != None:
                    await own_discord.reponse_send(ctx, "Cost register for Box selected")
                else:
                    await own_discord.reponse_send(ctx, "Error occured")

    @commands.command()
    @commands.guild_only()
    @commands.has_guild_permissions(administrator=True)
    async def addBoxCost(self, ctx:commands.Context, cost:int):
        boxs = self.boxs.fetchBoxOfGuild(self.findOrCreateGuild(ctx.guild.id).Guild)
        box = await Box.select(ctx, boxs)
        if box != None:
            moneys = self.moneys.fetchMoneyOfGuild(self.findOrCreateGuild(ctx.guild.id).Guild)
            if moneys == None or len(moneys) == 0:
                await own_discord.reponse_send(ctx, "Any Money registered")
            else:
                money = await Money.select(ctx, boxs)
                if money != None:
                    if self.boxs.addBoxCost(box.id, cost, money.id) != None:
                        await own_discord.reponse_send(ctx, "Cost register for Box selected")
                    else:
                        await own_discord.reponse_send(ctx, "Error occured")

    @commands.command()
    @commands.guild_only()
    async def showBoxs(self, ctx:commands.Context):
        boxs = self.boxs.fetchBoxOfGuild(self.findOrCreateGuild(ctx.guild.id).Guild)
        if boxs == None or len(boxs) == 0:
            await own_discord.reponse_send(ctx, "Any Box registered")
        else:
            await Box.display(ctx, boxs)

    @commands.command()
    @commands.guild_only()
    async def showBoxContent(self, ctx:commands.Context):
        boxs = self.boxs.fetchBoxOfGuild(self.findOrCreateGuild(ctx.guild.id).Guild)
        if boxs == None or len(boxs) == 0:
            await own_discord.reponse_send(ctx, "Any box exist in this guild.")
        box = await Box.select(ctx, boxs)
        if box != None:
            content = self.boxs.fetchContentBox(box.id)
            if content == None or len(content) == 0:
                await own_discord.reponse_send(ctx, "Any Content registered for this box")
            else:
                await BoxContent.sendList(ctx, content)

    @commands.command()
    @commands.guild_only()
    async def showBoxCosts(self, ctx:commands.Context):
        boxs = self.boxs.fetchBoxOfGuild(self.findOrCreateGuild(ctx.guild.id).Guild)
        box = await Box.select(ctx, boxs)
        if box != None:
            costs = self.boxs.fetchBoxCost(box.id)
            if costs is None or len(costs) == 0:
                await own_discord.reponse_send(ctx, "Any Cost registered for this box.")
            else:
                await BoxCost.sendList(ctx, costs)
    
    @commands.command()
    @commands.guild_only()
    async def showMoney(self, ctx:commands.Context):
        moneys = self.moneys.fetchMoneyOfGuild(self.findOrCreateGuild(ctx.guild.id).Guild)
        if moneys == None or len(moneys) == 0:
            await own_discord.reponse_send(ctx, "Any Money registered")
        else:
            await Money.sendList(ctx, moneys)

    @commands.command()
    @commands.guild_only()
    @commands.has_guild_permissions(administrator=True)
    async def showReward(self, ctx:commands.Context):
        rewards = self.rewards.fetchRewardOfGuild(self.findOrCreateGuild(ctx.guild.id).Guild)
        await Reward.display(ctx, rewards)

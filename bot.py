import time, logging, discord
from own import own_sqlite
from discord.ext import commands
from config import settings
from models import Guild, User
import cogs

logging.basicConfig(format='%(asctime)s:%(levelname)s:%(message)s', filename='logs/%s.log'%(str(time.time())), level=settings.DISCORD_LOG)

logging.info("Verification Database")
if (own_sqlite.bdd_exist(settings.DISCORD_DB) != True):
    logging.info("Creating Database")
    own_sqlite.bdd_create(settings.DISCORD_DB)

logging.info("Connecting Database")
bdd = own_sqlite.connect(settings.DISCORD_DB)

guilds = Guild(bdd)
users = User(bdd)

def determine_prefix(bot:commands.Bot, message:discord.Message):
    prefix = users.fetchPrefixOfUser(message.author.id)
    if message.guild and not prefix:
        prefix = guilds.fetchPrefixOfServer(message.guild.id)
    return settings.DISCORD_PREFIX if not prefix else prefix

bot = commands.Bot(command_prefix=determine_prefix)
bot.add_cog(cogs.ManagementCog(bdd))

@bot.event
async def on_ready():
    act = discord.Game(settings.DISCORD_HELP)
    await bot.change_presence(activity=act)
    logging.info("Bot Launch.")
    print("Bot Up !")

bot.run(settings.DISCORD_TOKEN)
from unittest import TestCase
from own.own_sqlite import bdd_create, connect
import models, os

class TestModels(TestCase):

    def test_event(self):
        path = "./tests/db_test.sqlite3"
        bdd_create(path)
        db = connect(path)
        users = models.User(db)
        guilds = models.Guild(db)
        moneys = models.Money(db, guilds)
        rewards = models.Reward(db, moneys)
        boxs = models.Box(db, moneys)
        events = models.Event(db, users, rewards)
        discordIdUser1 = 123
        user1 = users.addUser(discordIdUser1, "~")
        discordIdUser2 = 456
        user2 = users.addUser(discordIdUser2)
        discordIdGuild = 789
        guild = guilds.addGuild(discordIdGuild)
        money1 = moneys.addMoney(discordIdGuild, "Money", 123)
        money2 = moneys.addMoney(discordIdGuild, "Money2", 456)
        money3 = moneys.addMoney(discordIdGuild, "Money3", 789)
        reward1 = rewards.addReward(discordIdGuild, "RewardScoreExample", models.RewardType.SCORE, 50)
        rewards.addContentOfReward(reward1.id, money1.id)
        rewards.addContentOfReward(reward1.id, money2.id, 5)
        reward2 = rewards.addReward(discordIdGuild, "RewardMaxExample", models.RewardType.NEW_MAX)
        rewards.addContentOfReward(reward2.id, money3.id)
        rewards.addGlobalReward(reward2.id)
        reward3 = rewards.addReward(discordIdGuild, "RewardParticipationExample", models.RewardType.PARTICIPATION)
        event = events.addEvent(discordIdGuild, "EventBlap")
        events.addRewardToEvent(event.id, reward1.id)
        events.addRewardToEvent(event.id, reward3.id)
        instance = events.startEvent(event.id)
        events.addUserToEvent(event.id, discordIdUser1)
        events.addUserToEvent(event.id, discordIdUser2, 50)
        for user in events.fetchUsersOfEvent(event.id):
            self.assertIn(user.user, [user1.id, user2.id])
        rewardsInstance1 = events.extractRewardsOfInstance(instance.id)
        for reward in rewardsInstance1[2]:
            self.assertIn(reward.pattern, [reward1.pattern, reward2.pattern, reward3.pattern])
        events.updateScoreForUser(event.id, discordIdUser1, 45)
        rewardsInstance2 = events.extractRewardsOfInstance(instance.id)
        for reward in rewardsInstance2[2]:
            self.assertIn(reward.pattern, [reward1.pattern, reward2.pattern, reward3.pattern])
        events.updateScoreForUser(event.id, discordIdUser1, 55)
        rewardsInstance3 = events.extractRewardsOfInstance(instance.id)
        for reward in rewardsInstance3[1]:
            self.assertIn(reward.pattern, [reward1.pattern, reward2.pattern, reward3.pattern])
        for reward in rewardsInstance3[2]:
            self.assertIn(reward.pattern, [reward1.pattern, reward3.pattern])
        box = boxs.addBox(discordIdGuild, "BoxA")
        boxs.addBoxCost(box.id, 1, money1.id)
        contents = []
        for i in range(9):
            contents.append(boxs.addBoxContent(box.id, "%i" % (i + 1), i + 1, i + 1).pattern)
        for i in range(9):
            content = boxs.takeRandomBoxContent(box.id)
            self.assertIn(content.pattern, contents)
            contents.remove(content.pattern)
        os.remove(path)


from own.own_sqlite.models import Model, ModelData
from own.own_sqlite.fields import Integer, FieldRef, TextWithoutCaseSensitive
from own.own_sqlite.keys import ForeignKey, Default_PrimaryKey, Id
from models import Guild
import sqlite3

class Money:

    def __init__(self, db:sqlite3.Connection, guilds:Guild):
        self.db = db
        self.guilds = guilds
        fields = [
            Id,
            TextWithoutCaseSensitive("name", True),
            Integer("id_guild", True),
            Integer("id_emoji")
        ]
        foreign_key = [ForeignKey(fields[2], FieldRef(Id, self.guilds.model.name))]
        self.model = Model("Money", fields=fields, primary_key=Default_PrimaryKey, foreign_key=foreign_key, db=db)

    def addMoney(self, guild:int, name:str, emoji:int = None):
        data = self.guilds.fetchGuildByDiscordId(guild)
        if data:
            return self.model.insertOne(ModelData(self.model.fields, [None, name, data.id, emoji]), self.db)
        return None

    def fetchMoney(self, money:int):
        return self.model.fetchOneByField(money, Id, self.db)

    def fetchMoneyOfGuild(self, guild:int):
        data = self.guilds.fetchGuildByDiscordId(guild)
        if data:
            return self.model.fetchByField(data.id, self.model.fields[2], self.db)
        return None

    def fetchMoneyOfGuildWithName(self, guild:int, name:str):
        data = self.guilds.fetchGuildByDiscordId(guild)
        if data:
            return self.model.fetchOneByFields([data.id, name], [self.model.fields[2], self.model.fields[1]], self.db)
        return None

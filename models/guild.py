from own.own_sqlite.models import ModelData, Model
from own.own_sqlite.fields import Integer, Text
from own.own_sqlite.keys import Id, Default_PrimaryKey
import sqlite3

class Guild:

    def __init__(self, db:sqlite3.Connection):
        fields = [
            Id,
            Integer("Guild", True),
            Text("prefix")
        ]
        self.db = db
        self.model = Model("Guild", fields=fields, primary_key=Default_PrimaryKey, db=db)

    def addGuild(self, guild:int, prefix:str = None):
        if len(self.model.fetchByField(guild, self.model.fields[1], self.db)) == 0:
            data = ModelData(self.model.fields, [None, guild, prefix])
            return self.model.insertOne(data, self.db)
        return None

    def updateGuild(self, guild:int, prefix:str = None, verbose:bool = False):
        data = self.model.fetchByField(guild, self.model.fields[1], self.db)
        if data:
            for dat in data:
                dat.prefix = prefix if prefix else dat.prefix
                dat.Guild = guild
            return self.model.updateMany(data, self.db, verbose)
        return None

    def fetchGuild(self, guildId:int):
        return self.model.fetchOneByField(guildId, Id, self.db)

    def fetchGuildByDiscordId(self, guild:int):
        return self.model.fetchOneByField(guild, self.model.fields[1], self.db)

    def fetchPrefixOfServer(self, guild:int):
        request = self.model.fetchOneByField(guild, self.model.fields[1], self.db)
        return request.prefix if request else None
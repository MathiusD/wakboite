from models.reward import RewardType
from own.own_sqlite.models import Model, ModelData
from own.own_sqlite.fields import Text, Integer, FieldRef, TextWithoutCaseSensitive, Real
from own.own_sqlite.keys import ForeignKey, Default_PrimaryKey, Id
from models import Reward, User
import sqlite3, time

class Event:

    def __init__(self, db:sqlite3.Connection, users:User, rewards:Reward):
        self.db = db
        self.rewards = rewards
        self.guilds = self.rewards.guilds
        self.users = users
        fields = [
            Id,
            Integer("id_guild", True),
            TextWithoutCaseSensitive("name", True),
            Text("description"),
            Text("rules"),
            Integer("min", True),
            Integer("max", True)
        ]
        foreign_key = [ForeignKey(fields[1], FieldRef(Id, self.guilds.model.name))]
        self.model = Model("Event", fields=fields, primary_key=Default_PrimaryKey, foreign_key=foreign_key, db=db)
        fields_history = [
            Id,
            Integer("id_event", True),
            Real("started", True),
            Real("end"),
            Integer("min", True),
            Integer("max", True)
        ]
        foreign_key_history = [ForeignKey(fields_history[1], FieldRef(Id, self.model.name))]
        self.model_history = Model("EventHistory", fields=fields_history, primary_key=Default_PrimaryKey, foreign_key=foreign_key_history, db=db)
        fields_member = [
            Id,
            Integer("id_session", True),
            Integer("user", True),
            Integer("score")
        ]
        foreign_key_member = [
            ForeignKey(fields_member[1], FieldRef(Id, self.model_history.name)),
            ForeignKey(fields_member[2], FieldRef(Id, self.users.model.name))
        ]
        self.model_member = Model("EventMember", fields=fields_member, primary_key=Default_PrimaryKey, foreign_key=foreign_key_member, db=db)
        fields_rewards = [
            Id,
            Integer("id_event", True),
            Integer("id_reward", True)
        ]
        foreign_key_reward = [
            ForeignKey(fields_rewards[1], FieldRef(Id, self.model.name)),
            ForeignKey(fields_rewards[2], FieldRef(Id, self.rewards.model.name))
        ]
        self.model_reward = Model("EventReward", fields=fields_rewards, primary_key=Default_PrimaryKey, foreign_key=foreign_key_reward, db=db)

    def addEvent(self, guild:int, name:str, description:str = None, rules:str = None, min:int = 0, max:int = 0):
        data = self.guilds.fetchGuildByDiscordId(guild)
        if data:
            return self.model.insertOne(
                ModelData(
                    self.model.fields,
                    [None, data.id, name, description, rules, min, max]
                ),
                self.db
            )
        return None

    def fetchEventOfGuild(self, guild:int):
        data = self.guilds.fetchGuildByDiscordId(guild)
        if data:
            return self.model.fetchByField(data.id, self.model.fields[1], self.db)
        return None

    def fetchEventOfGuildWithName(self, guild:int, name:str):
        data = self.guilds.fetchGuildByDiscordId(guild)
        if data:
            return self.model.fetchOneByFields([data.id, name], [self.model.fields[1], self.model.fields[2]], self.db)
        return None

    def fetchEventById(self, event:int):
        return self.model.fetchOneByField(event, self.model.fields[0], self.db)

    def eventHasInstance(self, event:int):
        if self.fetchEventById(event):
            instances = self.model_history.fetchByFields(
                [event, 0],
                [self.model_history.fields[1], self.model_history.fields[3]],
                self.db
            )
            if len(instances) == 1:
                return instances[0]
        return False

    def startEvent(self, event:int):
        eventData = self.fetchEventById(event)
        if eventData:
            instance = self.eventHasInstance(event)
            if instance is False:
                instance = self.model_history.insertOne(
                    ModelData(
                        self.model_history.fields,
                        [None, event, time.time(), 0.0, eventData.min, eventData.max]
                    ),
                    self.db
                )
            return instance
        return None

    def extractMaxOfInstance(self, instanceId:int):
        instance = self.fetchInstance(instanceId)
        if instance is not None:
            newMin = None
            eventData = self.fetchEventById(instance.id_event)
            for user in self.fetchUsersOfInstance(instance.id):
                if user.score and user.score > eventData.max:
                    newMin = user.user
                    eventData.max = user.score
            if newMin is not None:
                return self.users.fetchUserById(newMin)
        return None

    def updateMax(self, event:int):
        instance = self.eventHasInstance(event)
        if instance is not False:
            user = self.extractMaxOfInstance(instance.id)
            if user:
                eventData = self.fetchEventById(event)
                eventData.max = user.score
                self.model.updateOne(eventData, self.db)
                return user
        return None

    def extractMinOfInstance(self, instanceId:int):
        instance = self.fetchInstance(instanceId)
        if instance is not None:
            newMin = None
            eventData = self.fetchEventById(instance.id_event)
            for user in self.fetchUsersOfInstance(instance.id):
                if user.score and user.score < eventData.min:
                    newMin = user.user
                    eventData.min = user.score
            if newMin is not None:
                return self.users.fetchUserById(newMin)
        return None

    def updateMin(self, event:int):
        instance = self.eventHasInstance(event)
        if instance is not False:
            user = self.extractMaxOfInstance(instance.id)
            if user:
                eventData = self.fetchEventById(event)
                eventData.min = user.score
                self.model.updateOne(eventData, self.db)
                return user
        return None

    def fetchInstance(self, instance:int):
        return self.model_history.fetchOneByField(instance, Id, self.db)

    def extractRewardsOfInstance(self, instanceId:int):
        instance = self.fetchInstance(instanceId)
        if instance:
            out = {}
            event = self.fetchEventById(instance.id_event)
            guild = self.guilds.fetchGuild(event.id_guild)
            rewards = self.rewards.fetchGlobalRewardOfGuild(guild.Guild)
            if rewards is None:
                rewards = []
            for rewardBase in self.fetchRewardsOfEvent(event.id):
                if rewardBase is not None:
                    reward = self.rewards.fetchReward(rewardBase.id_reward)
                    if reward:
                        rewards.append(reward)
            for user in self.fetchUsersOfInstance(instance.id):
                out[user.id] = []
            for reward in rewards:
                sortedUsers = self.fetchUsersOfInstanceSortedByScore(instance.id)
                if reward.condition_type == RewardType.NEW_MAX.value:
                    user = self.extractMaxOfInstance(instance.id)
                    if user:
                        out[user.id].append(reward)
                elif reward.condition_type == RewardType.NEW_MIN.value:
                    user = self.extractMinOfInstance(instance.id)
                    if user:
                        out[user.id].append(reward)
                elif reward.condition_type == RewardType.TOP.value:
                    if reward.condition_value > len(sortedUsers):
                        for user in sortedUsers[reward.condition_value]:
                            out[user.id].append(reward)
                elif reward.condition_type == RewardType.LAST.value:
                    if reward.condition_value > len(sortedUsers):
                        for user in sortedUsers[:-reward.condition_value]:
                            out[user.id].append(reward)
                else:
                    for user in self.fetchUsersOfInstance(instance.id):
                        if reward.condition_type == RewardType.SCORE.value:
                            if user.score and reward.condition_value <= user.score:
                                out[user.id].append(reward)
                        elif reward.condition_type == RewardType.PARTICIPATION.value:
                            out[user.id].append(reward)
            return out
        return None

    def closeEvent(self, event:int):
        maxUser = self.updateMax(event)
        minUser = self.updateMin(event)
        instance = self.endEvent(event)
        return {
            "instance":instance,
            "userMax":maxUser,
            "userMin":minUser
        }

    def endEvent(self, event:int):
        instance = self.eventHasInstance(event)
        if instance is not False:
            instance.end = time.time()
            return self.model_history.updateOne(instance, self.db)
        return None

    def userIsRegisterForEvent(self, event:int, userId:int):
        user = self.users.fetchUserByDiscordId(userId)
        instance = self.eventHasInstance(event)
        if instance is not False and user is not None:
            register = self.model_member.fetchOneByFields(
                [user.id, instance.id],
                [self.model_member.fields[2], self.model_member.fields[1]],
                self.db
            )
            if register is not None:
                return register
        return False

    def updateScoreForUser(self, event:int, userId:int, score:int = None):
        user = self.users.fetchUserByDiscordId(userId)
        if user is not None:
            instance = self.eventHasInstance(event)
            if instance is not False:
                register = self.userIsRegisterForEvent(event, userId)
                if register is False:
                    register = self.model_member.insertOne(
                        ModelData(
                            self.model_member.fields,
                            [None, instance.id, user.id, score]
                        ),
                        self.db
                    )
                elif register.score != score:
                    register.score = score
                    register = self.model_member.updateOne(
                        register,
                        self.db
                    )
                return register
        return None

    def addUserToEvent(self, event:int, userId:int, score:int = None):
        return self.updateScoreForUser(event, userId, score)

    def addRewardToEvent(self, eventId:int, rewardId:int):
        reward = self.rewards.fetchReward(rewardId)
        event = self.fetchEventById(eventId)
        if reward and event and self.rewards.isGlobalReward(reward.id) is False:
            data = self.model_reward.fetchOneByField(reward.id, self.model_reward.fields[2], self.db)
            if data is None:
                data = self.model_reward.insertOne(
                    ModelData(self.model_reward.fields, [None, event.id, reward.id]),
                    self.db
                )
            return data
        return None

    def fetchRewardsOfEvent(self, eventId:int):
        event = self.fetchEventById(eventId)
        if event:
            return self.model_reward.fetchByField(event.id, self.model_reward.fields[1], self.db)
        return None

    def fetchUsersOfEvent(self, event:int):
        instance = self.eventHasInstance(event)
        if instance:
            return self.fetchUsersOfInstance(instance.id)
        return None

    def fetchUsersOfInstance(self, instanceId:int):
        instance = self.fetchInstance(instanceId)
        if instance is not False:
            return self.model_member.fetchByField(
                instance.id,
                self.model_member.fields[1],
                self.db
            )
        return None

    def fetchUsersOfInstanceSortedByScore(self, instanceId:int):
        users = self.fetchUsersOfInstance(instanceId)
        if users:
            out = {}
            for user in users:
                if not user.score in out:
                    out[user.score] = [user]
                else:
                    out[user.score].append(user)
            return out
        return None

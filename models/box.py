from own.own_sqlite.models import Model, ModelData
from own.own_sqlite.fields import Text, Integer, FieldRef
from own.own_sqlite.keys import Id, Default_PrimaryKey, ForeignKey
from models import Money
import sqlite3, random

class Box:

    def __init__(self, db:sqlite3.Connection, moneys:Money):
        self.db = db
        self.moneys = moneys
        self.guilds = moneys.guilds
        fields = [
            Id,
            Text("name", True),
            Integer("id_guild", True)
        ]
        foreign_key = [ForeignKey(fields[2], FieldRef(Id, self.guilds.model.name))]
        self.model = Model("Box", fields=fields, primary_key=Default_PrimaryKey, foreign_key=foreign_key, db=db)
        fields_cost = [
            Id,
            Integer("box", True),
            Integer("cost", True),
            Integer("money", True)
        ]
        foreign_key_cost = [
            ForeignKey(fields_cost[1], FieldRef(Id, self.model.name)),
            ForeignKey(fields_cost[3], FieldRef(Id, self.moneys.model.name))
        ]
        self.model_cost = Model("CostBox", fields=fields_cost, primary_key=Default_PrimaryKey, foreign_key=foreign_key_cost, db=db)
        fields_content = [
            Id,
            Integer("box", True),
            Text("object", True),
            Integer("quantity", True),
            Integer("coef", True)
        ]
        foreign_key_content = [ForeignKey(fields_content[1], FieldRef(Id, self.model.name))]
        self.model_content = Model("ContentBox", fields=fields_content, primary_key=Default_PrimaryKey, foreign_key=foreign_key_content, db=db)

    def addBox(self, guild:int, name:str):
        data = self.guilds.fetchGuildByDiscordId(guild)
        if data:
            return self.model.insertOne(ModelData(self.model.fields, [None, name, data.id]), self.db)
        return None

    def addBoxContent(self, box:int, content:str, quantity:int = 1, coef:int = 1):
        if self.model.fetchOneByField(box, Id, self.db) is not None:
            return self.model_content.insertOne(ModelData(self.model_content.fields, [None, box, content, quantity, coef]), self.db)
        return None

    def removeBoxContent(self, box:int, id:int):
        if self.model.fetchOneByField(box, Id, self.db) is not None:
            return self.model_content.DelByFields([id, box], [self.model_content.fields[0], self.model_content.fields[1]], self.db)
        return None

    def takeRandomBoxContent(self, box:int):
        coefs = []
        maxRand = 0
        for content in self.fetchContentBox(box):
            coefs.append({"content":content, "target":range(maxRand, maxRand + content.coef)})
            maxRand += content.coef
        result = random.randint(0, maxRand - 1)
        for coef in coefs:
            if coefs.index(coef) != len(coefs) -1 or result in coef["target"]:
                content = coef["content"]
                self.removeBoxContent(box, content.id)
                return content
        return None

    def addBoxCost(self, box:int, cost:int, money:int):
        box = self.model.fetchOneByField(box, self.model.fields[0], self.db)
        if box:
            data = self.moneys.fetchMoney(money)
            if data and data.id_guild == box.id_guild:
                return self.model_cost.insertOne(ModelData(self.model_cost.fields, [None, box.id, cost, data.id]), self.db)
        return None

    def removeBoxCost(self, box:int, money:int):
        box = self.model.fetchOneByField(box, self.model.fields[0], self.db)
        if box:
            data = self.moneys.fetchMoney(money)
            if data and data.id_guild == box.id_guild:
                return self.model_cost.DelByFields([data.id, box], [self.model_cost.fields[1], self.model_cost.fields[3]], self.db)
        return None

    def fetchBoxOfGuild(self, guild:int):
        data = self.guilds.fetchGuildByDiscordId(guild)
        if data:
            return self.processBoxes(self.model.fetchByField(data.id, self.model.fields[2], self.db))
        return None

    def processBoxes(self, boxes:list):
        boxs = []
        for box in boxes:
            boxs.append(self.processBox(box))
        return boxs

    def processBox(self, box:ModelData):
        if box != None:
            box.content = self.fetchContentBox(box.id)
            box.cost = self.fetchBoxCost(box.id)
        return box

    def fetchBoxCost(self, box:int):
        if len(self.model.fetchByField(box, self.model.fields[0], self.db)) == 1:
            return self.processBoxCosts(self.model_cost.fetchByField(box, self.model_cost.fields[1], self.db))
        return None

    def fetchContentBox(self, box:int):
        if len(self.model.fetchByField(box, self.model.fields[0], self.db)) == 1:
            return self.model_content.fetchByField(box, self.model_content.fields[1], self.db)
        return None

    def processBoxCosts(self, costs:list):
        out = []
        for cost in costs:
            out.append(self.processBoxCost(cost))
        return out


    def processBoxCost(self, cost:ModelData):
        if cost != None:
            cost.money = self.moneys.fetchMoney(cost.money)
        return cost
from own.own_sqlite.models import Model, ModelData
from own.own_sqlite.fields import TextWithoutCaseSensitive, Integer, FieldRef
from own.own_sqlite.keys import ForeignKey, Default_PrimaryKey, Id
from models import Money
from enum import Enum
import sqlite3

class RewardType(Enum):
    SCORE = 0
    NEW_MAX = 1
    NEW_MIN = 2
    TOP = 3
    LAST = 4
    PARTICIPATION = 5

types = [
    RewardType.SCORE,
    RewardType.NEW_MAX,
    RewardType.NEW_MIN,
    RewardType.TOP,
    RewardType.LAST,
    RewardType.PARTICIPATION
]

class Reward:

    def __init__(self, db:sqlite3.Connection, moneys:Money):
        self.db = db
        self.moneys = moneys
        self.guilds = moneys.guilds
        fields = [
            Id,
            Integer("id_guild", True),
            TextWithoutCaseSensitive("name", True),
            Integer("condition_type", True),
            Integer("condition_value")
        ]
        foreign_key = [ForeignKey(fields[1], FieldRef(Id, self.guilds.model.name))]
        self.model = Model("Reward", fields=fields, primary_key=Default_PrimaryKey, foreign_key=foreign_key, db=db)
        fields_content = [
            Id,
            Integer("id_reward", True),
            Integer("money", True),
            Integer("quantity", True)
        ]
        foreign_key_content = [
            ForeignKey(fields_content[1], FieldRef(Id, self.model.name)),
            ForeignKey(fields_content[2], FieldRef(Id, self.moneys.model.name))
        ]
        self.model_content = Model("RewardContent", fields=fields_content, primary_key=Default_PrimaryKey, foreign_key=foreign_key_content, db=self.db)
        fields_global = [
            Id,
            Integer("id_reward", True)
        ]
        foreign_key_global = [
            ForeignKey(fields_global[1], FieldRef(Id, self.model.name))
        ]
        self.model_global = Model("RewardGlobal", fields=fields_global, foreign_key=foreign_key_global, db=db)

    def addReward(self, guild:int, name:str, conditionType:int = RewardType.SCORE, conditionValue:int = None):
        data = self.guilds.fetchGuildByDiscordId(guild)
        if data:
            return self.model.insertOne(
                ModelData(
                    self.model.fields,
                    [None, data.id, name, conditionType.value if isinstance(conditionType, RewardType) else conditionType, conditionValue]
                ),
                self.db
            )
        return None

    def fetchReward(self, reward:int):
        return self.model.fetchOneByField(reward, Id, self.db)

    def isGlobalReward(self, rewardId:int):
        reward = self.fetchReward(rewardId)
        if reward and self.model_global.fetchByField(reward.id, self.model_global.fields[1], self.db):
                return True
        return False

    def addGlobalReward(self, rewardId:int):
        reward = self.fetchReward(rewardId)
        if reward and self.isGlobalReward(reward.id) is False:
            self.model_global.insertOne(ModelData(self.model_global.fields, [None, reward.id]), self.db)
            return True
        return False

    def removeGlobalReward(self, rewardId:int):
        reward = self.fetchReward(rewardId)
        if reward and self.isGlobalReward(reward.id) is True:
            self.model_global.DelByField(reward.id, self.model_global.fields[1], self.db)
            return True
        return False

    def fetchGlobalRewardOfGuild(self, guild:int):
        rewards = self.fetchRewardOfGuild(guild)
        if rewards:
            out = []
            for reward in rewards:
                if self.isGlobalReward(reward.id):
                    out.append(reward)
            return out
        return None

    def addContentOfReward(self, reward:int, money:int, quantity:int = 1):
        rewardData = self.fetchReward(reward)
        moneyData = self.moneys.fetchMoney(money)
        if rewardData and moneyData:
            oldData = self.model_content.fetchOneByFields(
                [rewardData.id, moneyData.id],
                [
                    self.model_content.fields[1],
                    self.model_content.fields[2]
                ],
                self.db
            )
            if oldData:
                data = oldData
                data.quantity += quantity
                return self.model_content.updateOne(data, self.db)
            else:
                return self.model_content.insertOne(
                    ModelData(
                        self.model_content.fields,
                        [None, rewardData.id, moneyData.id, quantity]
                    ),
                    self.db
                )
        return None

    def fetchContentOfReward(self, reward:int):
        rewardData = self.fetchReward(reward)
        if rewardData:
            return self.model_content.fetchByField(rewardData.id, self.model_content.fields[1], self.db)
        return None

    def fetchRewardOfGuild(self, guild:int):
        data = self.guilds.fetchGuildByDiscordId(guild)
        if data:
            return self.model.fetchByField(data.id, self.model.fields[1], self.db)
        return None

    def fetchRewardOfGuildWithName(self, guild:int, name:str):
        data = self.guilds.fetchGuildByDiscordId(guild)
        if data:
            return self.model.fetchOneByFields(
                [data.id, name],
                [self.model.fields[1], self.model.fields[2]],
                self.db
            )
        return None

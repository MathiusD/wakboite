FROM python:latest

WORKDIR /usr/docker/wakboite

COPY . .

RUN make rebuild

CMD ["make", "start"]
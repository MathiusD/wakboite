#!/bin/bash
# Setup This Repo
echo "Setup Repo";
FILE=./config/settings.py
if ! [ -f "$FILE" ]; then
    cp ./config/settings.sample.py $FILE;
fi
FOLDER=./data
if ! [ -f "$FOLDER" ]; then
    mkdir $FOLDER;
fi
mkdir logs;
git clone https://gitlab.com/MathiusD/owninstall;
cp owninstall/install.py own_install.py;
python3 build.py
rm -rf owninstall;